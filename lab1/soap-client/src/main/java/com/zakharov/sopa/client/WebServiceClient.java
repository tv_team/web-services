/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zakharov.sopa.client;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebServiceClient {

    public static void main(String[] args) throws MalformedURLException {
		URL url = new URL("http://localhost:8080/PersonService?wsdl");
		PersonService personService = new PersonService(url);
		Person search = new Person();
		try{
			List<Person> persons = personService.getPersonWebServicePort().getPersons();
			for (Person person : persons) {
				System.out.println("firstname: " + person.getFirstname()
						+ ", secondname: " + person.getSecondname() + ", gender: " + person.getGender()
						+ ", phonenumber: " + String.valueOf(person.getPhonenumber()) );
			}
			catch (IllegalNameException ex) {
				Logger.getLogger(WebServiceClient.class.getName()).log(Level.SEVERE, null, ex);
				System.out.println("Total persons: " + persons.size());

			}

			System.out.println("Total persons: " + persons.size());
			Integer status1 = personService.getPersonWebServicePort().deletePerson(8);
			System.out.println("Delete status is " + status1);
			Integer status2 = personService.getPersonWebServicePort().updatePerson("Vaca", "Paca","male", "222222", 3);
			System.out.println("Delete status is " + status2);
			Integer newId = personService.getPersonWebServicePort().insertPerson("Saraka", "Trakaka", "feemale" , "754312");
			System.out.println("New id is " + newId);
		}

	}
}
