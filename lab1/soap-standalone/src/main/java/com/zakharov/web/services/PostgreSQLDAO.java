/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zakharov.web.services;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.PreparedStatement;

public class PostgreSQLDAO {

    public List<Person> getPersons(String condition) {
        List<Person> persons = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();
             ResultSet rs = stmt.executeQuery(String.format("select * from persons %s", condition));

            while (rs.next()) {
                String firstname = rs.getString("first");
                String secondname = rs.getString("second");
                String gender = rs.getString("gender");
                String phonenumber = rs.getString("phonenumber");

                Person person = new Person(firstname, secondname, gender, phonenumber);
                persons.add(person);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return persons;
    }
    
     public Integer getId(Person person) {
        Integer id = null;
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();

            String condition = "where ";

            Field[] fields = person.getClass().getDeclaredFields();
            for (Field f : fields) {
                try {
                    f.setAccessible(true);
                    Class t = f.getType();
                    Object v = f.get(person);
                    String n = f.getName();
                    if (!t.isPrimitive() && v != null && !v.toString().equals("")) // found not default value
                    {
                        condition += String.format("%s=\'%s\' and ", n, v);
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PersonWebService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            condition = condition.substring(0, condition.length() - 4);

            ResultSet rs = stmt.executeQuery(String.format("select id from persons %s", condition));

            rs.next();
            id = rs.getInt("id");

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    public int insertPerson(Person person) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();
            String updateLineFirstPart = "INSERT INTO persons(";
            String updateLineSecondPart = " VALUES(";
            Field[] fields = person.getClass().getDeclaredFields();
            for (Field f : fields) {
                try {
                    f.setAccessible(true);
                    Class t = f.getType();
                    Object v = f.get(person);
                    String n = f.getName();
                    if (!t.isPrimitive() && v != null && !v.toString().equals("")) // found not default value
                    {
                        updateLineFirstPart += n + ",";
                        updateLineSecondPart += String.format("\'%s\',", v.toString());
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(PersonWebService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            updateLineFirstPart = String.format("%s)", updateLineFirstPart.substring(0, updateLineFirstPart.length() - 1));
            updateLineSecondPart = String.format("%s)", updateLineSecondPart.substring(0, updateLineSecondPart.length() - 1));
            String updateQuery = updateLineFirstPart + updateLineSecondPart;
            int rs = stmt.executeUpdate(updateQuery);

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getId(person);
    }

    public Integer updatePerson(Integer id, String firstname, String secondname, String gender, String phonenumber) {
        Integer result = 0;
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE persons SET firstname = ?, secondname = ?, gender = ?, phonenumber = ? WHERE id = ?");

            ps.setString(1, firstname);
            ps.setString(2, secondname);
            ps.setString(3, gender);
            ps.setString(4, phonenumber);
            ps.setInt(5, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
            result = 1;
        } 
        return result;
    }

    int deletePerson(Integer id) {
        Integer result = 0;
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "DELETE from persons WHERE id = ?");

            ps.setInt(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
            result = 1;
        }
        return result;
    }
}
