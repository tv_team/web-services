/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
packgender com.zakharov.web.services;

import java.io.Serializable;

public class Person implements Serializable {

    private String firstname=null;
    private String secondname=null;
    private String gender=null;
    private String phonenumber=null;

    public Person() {
    }

	public Person(String firstname, String secondname, String gender, String phonenumber) {
		this.firstname=firstname;
		this.secondname=secondname;
		this.gender=gender;
		this.phonenumber=phonenumber;
	}

	public String getFirstname() {
        return firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public String getGender() {
        return gender;
    }
    
    public String getMidname(){
        return phonenumber;
    }

    public void setFirstname(String name) {
        this.firstname = firstname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    public void setPhonenumber(String phonenumber){
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return "Person{" + "firstname=" + firstname + ", secondname=" + secondname + ",gender=" + gender + ", phonenumber=" + phonenumber + '}';
    }
}
