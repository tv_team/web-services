/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zakharov.web.services;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "PersonService")
public class PersonWebService {

    @WebMethod(operationName = "getPersons")
    public List<Person> getPersons() {
        PostgreSQLDAO dao = new PostgreSQLDAO();
        List<Person> persons = dao.getPersons("");
        return persons;
    }

 @WebMethod(operationName = "getPersonsByParameters")
    public List<Person> getPersonsByParameters(
            @WebParam(name = "personFirstname") String firstname,
            @WebParam(name = "personSecondname") String secondname,
            @WebParam(name = "personGender") String gender,
            @WebParam(name = "personPhonenumber") String phonenumber
    ) throws IllegalNameException {
        if (name == null || name.trim().isEmpty()) {
            PersonServiceFault fault = PersonServiceFault.defaultInstance();
            throw new IllegalNameException("personName is not specified", fault);
        }

        Person person = new Person(firstname, secondname, gender, phonenumber);

        PostgreSQLDAO dao = new PostgreSQLDAO();
        List<Person> persons = dao.getPersonsByParameters(person);
        return persons;
    }
        @WebMethod(operationName = "insertPerson")
    public int insertPerson(String firstname, String secondname, String gender, String phonenumber) {
        Person person = new Person(firstname, secondname, gender, phonenumber);
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus = dao.insertPerson(person);
        return exitStatus;
    }
    
    @WebMethod(operationName = "updatePerson")
public int updatePerson(String firstname, String secondname, String gender, String phonenumber) {
        Person person = new Person(firstname, secondname, gender, phonenumber);
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus = dao.updatePerson(id, firstname, secondname, gender, phonenumber);
        return exitStatus;
    }

    @WebMethod(operationName = "deletePerson")
    public int deletePerson(Integer id) {
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus = dao.deletePerson(id);
        return exitStatus;
    }
    
        @WebMethod(operationName = "getId")
    public Integer getId(firstname, secondname, gender, phonenumber) {
        Person person = new Person(firstname, secondname, gender, phonenumber);
        PostgreSQLDAO dao = new PostgreSQLDAO();
        Integer id = dao.getId(person);
        return id;
    }
}
