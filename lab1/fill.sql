CREATE TABLE "persons" (
 id bigserial NOT NULL,
 firstname character varying(200),
 secondname character varying(200),
 gender character varying(200),
 phonenumber character varying(200),
 CONSTRAINT "Persons_pkey" PRIMARY KEY (id)
);

INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Albert', 'Ivanov', 'male', '123456');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Petr', 'Sidorov', 'male', '838382');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Kachok', 'Struchok', 'male', '122456');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Nemo', 'Nona', 'male', '123124');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Kak', 'Trata', 'male', '123122');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Kto', 'Trototo', 'female', '123486');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Gde', 'Mi', 'male', '123456');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Ona', 'Vesem', 'female', '223456');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Ololo', 'Ssoboi', 'female', '173456');
INSERT INTO persons(firstname, secondname, gender, phonenumber) values ('Antonio', 'Kota', 'male', '123459');
